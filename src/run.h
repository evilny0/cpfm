/*
 * Copyright (c) 2018, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef CPFM_RUN_H_INCLUDED
#define CPFM_RUN_H_INCLUDED

#include <time.h>
#include <pthread.h>

class Runnable
{
public:
    Runnable() { m_bStopThread = false; };
    ~Runnable() { };

    void startThread()
    {
        m_threadObj = this;
        pthread_create (&m_threadId, NULL, &threadMethod, m_threadObj);
    }

    virtual void stopThread()
    {
        // maybe something here
        m_bStopThread = true;
        pthread_join (m_threadId,NULL);
    }

    bool isTimeReached (time_t timeToCheck)
    {
        time_t timeNow = time(NULL);
        return (timeNow > timeToCheck);
    }

protected:
    virtual void run() = 0;

    static void* threadMethod(void* arg)
    {
        ((Runnable*)arg)->run();
        return NULL;
    }

    Runnable* m_threadObj;
    bool m_bStopThread;
    pthread_t m_threadId;
};


#endif // CPFM_RUN_H_INCLUDED
