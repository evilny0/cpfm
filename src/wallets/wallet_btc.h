/*
 * Copyright (c) 2018, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef CPFM_WALLET_BTC_H_INCLUDED
#define CPFM_WALLET_BTC_H_INCLUDED

#include "wallets/wallet_type_btc.h"
#include "datasources/datasource.h"

class WalletBTC : public WalletTypeBTC
{
public:
    WalletBTC(int walletId):WalletTypeBTC(walletId) { m_coinId = CPFM_COIN_ID_BTC; m_blockchainId = CPFM_BLOCKCHAIN_ID_BTC; m_walletTypeId = CPFM_WALLET_TYPE_ID_BTC; }

protected:
    virtual list<BlockchainTxDetailsTypeBTC> getTxDetailsListForAddresses(list<string> addresses);
};

#endif // CPFM_WALLET_BTC_H_INCLUDED