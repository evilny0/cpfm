/*
 * Copyright (c) 2021, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef CPFM_WALLET_TYPE_ETH_H_INCLUDED
#define CPFM_WALLET_TYPE_ETH_H_INCLUDED

#include "wallet.h"
#include "datasources/datasource.h"

class WalletTypeETH : public Wallet
{
public:
    WalletTypeETH(int walletId):Wallet(walletId) {}

    virtual void update();

private:
    void addTxDetailsListToRawDB(const int walletId, const list<BlockchainTxDetailsTypeETH>& l);
    void addTxDetailsListToDB(const int walletId, string address, const list<BlockchainTxDetailsTypeETH>& l);
    list<BlockchainTxDetailsTypeETH> getTxDetailsListFromRawDB(const int walletId);
    void updateBalanceFromTxDetailsInDB(int walletId);

protected:
    virtual list<BlockchainTxDetailsTypeETH> getTxDetailsListForAddress(string address) = 0;

    int m_walletTypeId;
    int m_blockchainId;
    int m_coinId;
};

#endif // CPFM_WALLET_TYPE_ETH_H_INCLUDED