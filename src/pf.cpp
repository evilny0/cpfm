/*
 * Copyright (c) 2018, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "pf.h"
#include "wallet.h"
#include "exchange.h"
#include "exchanges/kraken.h"
#include "pricesource.h"


PortfolioManager::PortfolioManager()
{
    
}

PortfolioManager::~PortfolioManager()
{

}

void PortfolioManager::doTestStuff()
{
    // Test user 1
    emptyUserBalances(1);

//    ExchangesManager exchangesManager(1);
//    exchangesManager.analyzeUserAccounts();

    WalletsManager walletsManager(1);
    walletsManager.analyzeUserWallets();

    displayUserBalances(1);

    // Test user 2
    /*
    emptyUserBalances(2);

    ExchangesManager exchangesManagerTwo(2);
    exchangesManagerTwo.analyzeUserAccounts();

    WalletsManager walletsManagerTwo(2);
    walletsManagerTwo.analyzeUserWallets();

    displayUserBalances(2);
    */
}

void PortfolioManager::emptyUserBalances(int userId)
{
    mysql::connection db(getMysqlConfig());
    const auto wallets_balances = TableWalletsBalances{};
    const auto exchanges_balances = TableExchangesBalances{};

    db.run(remove_from(wallets_balances).unconditionally());
    linfo << "Emptied wallets balances DB table for user " << userId;

    db.run(remove_from(exchanges_balances).unconditionally());
    linfo << "Emptied exchanges balances DB table for user " << userId;
}

void PortfolioManager::displayUserBalances(int userId)
{
    mysql::connection db(getMysqlConfig());
    const auto wallets = TableWallets{};
    const auto wallets_balances = TableWalletsBalances{};
    const auto exchanges_accounts = TableExchangesAccounts{};
    const auto exchanges_balances = TableExchangesBalances{};

    map<int,Money> totalBalances;
    for (const auto& row: db.run(select(wallets_balances.balance, wallets_balances.coin_id).from(wallets_balances.cross_join(wallets)).where(wallets_balances.wallet_id == wallets.wallet_id and wallets.user_id == userId)))
    {
        Money m(row.balance);
        totalBalances[row.coin_id] += m;
    }

    for (const auto& row: db.run(select(exchanges_balances.balance, exchanges_balances.coin_id).from(exchanges_balances.cross_join(exchanges_accounts)).where(exchanges_balances.account_id == exchanges_accounts.account_id and exchanges_accounts.user_id == userId)))
    {
        Money m(row.balance);
        totalBalances[row.coin_id] += m;
    }

    Money total;
    PriceSourceCryptoWatch ps;
    for (const auto& balance: totalBalances)
    {
        if (balance.second != 0)
        {
            cout << getCoinName(balance.first) << " : " << balance.second << " | " << balance.second * ps.getCoinPrice(balance.first) << endl;
            total += balance.second * ps.getCoinPrice(balance.first);
        }
    }
    cout << "---------------" << endl;
    cout << "Total : " << total << " EUR." << endl;
}

void PortfolioManager::run()
{
    while (!m_bStopThread)
    {
        if (m_bStopThread)
            break;

#ifdef _WIN32
        SwitchToThread();
        Sleep (1000);
#else
        pthread_yield();
        sleep (1);
#endif
    }
}

string getCoinName(int coinId)
{
    string s;

    switch(coinId)
    {
        case CPFM_COIN_ID_BTC:
            s = "BTC";
            break;
        case CPFM_COIN_ID_LTC:
            s = "LTC";
            break;
        case CPFM_COIN_ID_EUR:
            s = "EUR";
            break;
        case CPFM_COIN_ID_ICN:
            s = "ICN";
            break;
        case CPFM_COIN_ID_ETC:
            s = "ETC";
            break;
        case CPFM_COIN_ID_ETH:
            s = "ETH";
            break;
        case CPFM_COIN_ID_BCH:
            s = "BCH";
            break;
        case CPFM_COIN_ID_BTG:
            s = "BTG";
            break;
        case CPFM_COIN_ID_LPT:
            s = "LPT";
            break;
        case CPFM_COIN_ID_BCDT:
            s = "BCDT";
            break;
        case CPFM_COIN_ID_AIR:
            s = "AIR";
            break;
        case CPFM_COIN_ID_AAVE:
            s = "AAVE";
            break;
        case CPFM_COIN_ID_LEND:
            s = "LEND";
            break;
        case CPFM_COIN_ID_KNC:
            s = "KNC";
            break;
        default:
            s = "Error!";
            break;
    }

    return s;
}
