/*
 * Copyright (c) 2018, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "pricesource.h"

PriceSource::PriceSource()
{
    
}

PriceSource::~PriceSource()
{

}

Money PriceSource::getCoinPrice(int coinId)
{
    getData();
    return getCoinPriceFromData(coinId);
}

PriceSourceCryptoWatch::PriceSourceCryptoWatch()
{
    
}

PriceSourceCryptoWatch::~PriceSourceCryptoWatch()
{

}

string PriceSourceCryptoWatch::getCacheFilename()
{
    return "data/cache/cryptowatch.prices";
}

void PriceSourceCryptoWatch::getData()
{
    if (bfs::exists(getCacheFilename()))
        return;

    try
    {
        string sRequestURL = "/markets/prices";
        http_client apiclient("https://api.cryptowat.ch");
        apiclient.request(methods::GET,sRequestURL).then([](http_response response)
        {
            if (response.status_code() == status_codes::OK)
            {
                return response.extract_json();
            }
            return pplx::task_from_result(json::value());
        })
        .then([this](pplx::task<json::value> previousTask)
        {
            ofstream f;
            f.open(getCacheFilename());
            f << previousTask.get();
            f.close();
        })
        .wait();
    }
    catch(const http::http_exception& e)
    {
        lerr << "Failed to query cryptowat.ch prices";
    }
}

Money PriceSourceCryptoWatch::getCoinPriceFromData(int coinId)
{
    ifstream f;
    f.open(getCacheFilename());
    json::value jvalue = json::value::parse(f);
    f.close();

    Money m;

    switch (coinId)
    {
        case CPFM_COIN_ID_BTC:
        {
            Money x (jvalue["result"]["market:kraken:btceur"]);
            m = x;
            break;
        }
        case CPFM_COIN_ID_LTC:
        {
            Money x (jvalue["result"]["market:kraken:ltceur"]);
            m = x;
            break;
        }
        case CPFM_COIN_ID_EUR:
        {
            m = 1;
            break;
        }
        case CPFM_COIN_ID_ICN:
        {
            Money x (jvalue["result"]["market:kraken:icnbtc"]);
            Money y (jvalue["result"]["market:kraken:btceur"]);
            // ldebug << "ICN is not quoted in EUR. BTC price is : " << x;
            m = x*y;
            break;
        }
        case CPFM_COIN_ID_ETC:
        {
            Money x (jvalue["result"]["market:kraken:etceur"]);
            m = x;
            break;
        }
        case CPFM_COIN_ID_ETH:
        {
            Money x (jvalue["result"]["market:kraken:etheur"]);
            m = x;
            break;
        }
        case CPFM_COIN_ID_BCH:
        {
            Money x (jvalue["result"]["market:kraken:bcheur"]);
            m = x;
            break;
        }
        case CPFM_COIN_ID_BTG:
        {
            Money x (jvalue["result"]["market:bitfinex:btgbtc"]);
            Money y (jvalue["result"]["market:kraken:btceur"]);
            // ldebug << "BTG is not quoted in EUR. BTC price is : " << x;
            m = x*y;
            break;
        }
        case CPFM_COIN_ID_KNC:
        {        
            Money x (jvalue["result"]["market:binance:kncbtc"]);
            Money y (jvalue["result"]["market:kraken:btceur"]);
            // ldebug << "KNC is not quoted in EUR. BTC price is : " << x;
            m = x*y;
            break;
        }
        case CPFM_COIN_ID_AIR:
        {
            // ldebug << "AIR is not quoted. Returning 0.";
            m = 0;
            break;
        }
        case CPFM_COIN_ID_LEND:
        {
            Money x (jvalue["result"]["market:kraken:aaveeur"]);
            // ldebug << "LEND is not quoted. Using AAVE.";
            m = x/100;
            break;
        }
        case CPFM_COIN_ID_AAVE:
        {
            Money x (jvalue["result"]["market:kraken:aaveeur"]);
            m = x;
            break;
        }
        case CPFM_COIN_ID_LPT:
        {
            Money x (jvalue["result"]["market:poloniex:lptbtc"]);
            Money y (jvalue["result"]["market:kraken:btceur"]);
            // ldebug << "LPT is not quoted in EUR. BTC price is : " << x;
            m = x*y;
            break;
        }
        case CPFM_COIN_ID_BCDT:
        {
            // ldebug << "BCDT is not quoted. Returning 0.";
            m = 0;
            break;
        }
        default:
        {
            m = 0;
            lerr << "Unsupported coin: " << coinId << ". Returning 0.";
            break;
        }
    }

    return m;
}
