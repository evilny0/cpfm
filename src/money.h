/*
 * Copyright (c) 2018, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef CPFM_MONEY_H_INCLUDED
#define CPFM_MONEY_H_INCLUDED

#include <sstream>
#include <boost/multiprecision/gmp.hpp>
#include <cpprest/json.h>

using namespace std;
namespace bmp = boost::multiprecision;

class Money
{
public:
    Money();
    Money(bmp::mpf_float_50 x);
    Money(const std::string& s);
    Money(const web::json::value& x);
    Money(int x);
    virtual ~Money();

    Money& operator=(const bmp::mpf_float_50& x);
    Money& operator=(const std::string& s);
    Money& operator=(const int& x);

    Money& operator+=(const Money& x);
    friend Money operator+(Money a, const Money& b) { a += b; return a; }

    Money& operator-=(const Money& x);
    friend Money operator-(Money a, const Money& b) { a -= b; return a; }

    Money& operator*=(const Money& x);
    friend Money operator*(Money a, const Money& b) { a *= b; return a; }

    Money& operator/=(const Money& x);
    friend Money operator/(Money a, const Money& b) { a /= b; return a; }
    
    //Money operator-() { return Money(-m_amount); }

    friend bool operator==(const Money& a, const int& b);
    friend bool operator!=(const Money& a, const int& b) { return !operator==(a,b); }
    friend bool operator<(const Money& a, const int& b);
    friend bool operator>(const Money& a, const int& b) { return operator<(b,a); }
    friend bool operator<=(const Money& a, const int& b) { return !operator>(a,b); }
    friend bool operator>=(const Money& a, const int& b) { return !operator<(a,b); }

    friend bool operator==(const Money& a, const Money& b);
    friend bool operator!=(const Money& a, const Money& b) { return !operator==(a,b); }
    friend bool operator<(const Money& a, const Money& b);
    friend bool operator>(const Money& a, const Money& b) { return operator<(b,a); }
    friend bool operator<=(const Money& a, const Money& b) { return !operator>(a,b); }
    friend bool operator>=(const Money& a, const Money& b) { return !operator<(a,b); }

    friend std::ostream& operator<<(std::ostream& os, const Money& obj);

    bmp::mpf_float_50& toBoostMpf() { return m_amount; }

private:
    bmp::mpf_float_50 m_amount;
};

#endif // CPFM_MONEY_H_INCLUDED