/*
 * Copyright (c) 2018, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef CPFM_EXCHANGE_KRAKEN_H_INCLUDED
#define CPFM_EXCHANGE_KRAKEN_H_INCLUDED

#include "exchange.h"

#define KRAKEN_API_URL "https://api.kraken.com"

class ExchangeHandlerKraken : public ExchangeHandler
{
public:
    ExchangeHandlerKraken(int userId, int accountId);
    virtual ~ExchangeHandlerKraken();

    virtual void analyzeUserData();

private:
    __int64 getNonce();
    string getSignature(const string& sRequestURL, const __int64& nonce, const string& sBody);
    string getCacheFilename(string operation);

    void getAccountBalance();
    void getAccountBalanceFromAPI();

    void getAccountLedgers();
    void getAccountLedgersFromAPI();

    void getAccountTrades();
    void getAccountTradesFromAPI();

    void analyzeAccountBalance();
    void analyzeAccountBalanceJSON(json::value jvalue);

    void getAccountDataFromDB();

    string m_apiKey;
    string m_apiPrivateKey;

    vector<unsigned char> sha256(const string& data);
    vector<unsigned char> b64_decode(const string& data);
    string b64_encode(const vector<unsigned char>& data);
    vector<unsigned char> hmac_sha512(const vector<unsigned char>& data, const vector<unsigned char>& key);
};

#endif // CPFM_EXCHANGE_KRAKEN_H_INCLUDED