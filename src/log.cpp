/*
 * Copyright (c) 2018, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include <iostream>
#include <fstream>
#include <chrono>
#include "log.h"

using namespace cpfm;
using namespace log;

const char* ErrorLogger::name() { return "ERROR"; }
const char* ErrorLogger::color() { return L_Red; }
const char* WarningLogger::name() { return "WARN "; }
const char* WarningLogger::color() { return L_Yellow; }
const char* InfoLogger::name() { return "INFO "; }
const char* InfoLogger::color() { return L_White; }
const char* DebugLogger::name() { return "DEBUG"; }
const char* DebugLogger::color() { return L_Teal; }
const char* TraceLogger::name() { return "TRACE"; }
const char* TraceLogger::color() { return L_Violet; }

LoggerBase::LoggerBase()
{
    m_logFileName = "pf.log";
}

LoggerBase::~LoggerBase()
{
    writeLogToOutput();
    //writeLogToFile();
}

string LoggerBase::getDateTimeString()
{
    time_t rawTime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    char buf[100];
    if (strftime(buf, 100, "%y-%m-%d %T", localtime(&rawTime)) == 0)
        buf[0] = 0;

    string s(buf);

    return s;
}

void LoggerBase::writeLogToOutput()
{
    cout << m_buffer.str() << L_Reset << endl;
}

void LoggerBase::writeLogToFile()
{
    ofstream f(m_logFileName,ios::app);

    if (!f.fail())
    {
        f << m_buffer.str() << L_Reset << endl;
        f.close();    
    }
}
