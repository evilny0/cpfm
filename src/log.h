/*
 * Copyright (c) 2018, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef CPFM_LOG_H_INCLUDED
#define CPFM_LOG_H_INCLUDED

#include <string>
#include <sstream>
#include <chrono>
#include <iostream>

using namespace std;

#define llog(X) cpfm::log::Logger<X>()
#define lerr llog(cpfm::log::ErrorLogger)
#define linfo llog(cpfm::log::InfoLogger)
#define lwarn llog(cpfm::log::WarningLogger)
#define ldebug llog(cpfm::log::DebugLogger)
#define ltrace llog(cpfm::log::TraceLogger)

namespace cpfm { namespace log {

#define L_Reset "\x1b[0m"       // Text Reset

// Regular Colors
#define L_Black "\x1b[30m"      // Black
#define L_Coal "\x1b[90m"       // Black
#define L_Gray "\x1b[37m"       // White
#define L_White "\x1b[97m"      // White
#define L_Maroon "\x1b[31m"     // Red
#define L_Red "\x1b[91m"        // Red
#define L_Green "\x1b[32m"      // Green
#define L_Lime "\x1b[92m"       // Green
#define L_Orange "\x1b[33m"     // Yellow
#define L_Yellow "\x1b[93m"     // Yellow
#define L_Navy "\x1b[34m"       // Blue
#define L_Blue "\x1b[94m"       // Blue
#define L_Violet "\x1b[35m"     // Purple
#define L_Purple "\x1b[95m"     // Purple
#define L_Teal "\x1b[36m"       // Cyan
#define L_Cyan "\x1b[96m"       // Cyan

#define L_BlackBold "\x1b[1;30m"     // Black
#define L_CoalBold "\x1b[1;90m"      // Black
#define L_GrayBold "\x1b[1;37m"      // White
#define L_WhiteBold "\x1b[1;97m"     // White
#define L_MaroonBold "\x1b[1;31m"    // Red
#define L_RedBold "\x1b[1;91m"       // Red
#define L_GreenBold "\x1b[1;32m"     // Green
#define L_LimeBold "\x1b[1;92m"      // Green
#define L_OrangeBold "\x1b[1;33m"    // Yellow
#define L_YellowBold "\x1b[1;93m"    // Yellow
#define L_NavyBold "\x1b[1;34m"      // Blue
#define L_BlueBold "\x1b[1;94m"      // Blue
#define L_VioletBold "\x1b[1;35m"    // Purple
#define L_PurpleBold "\x1b[1;95m"    // Purple
#define L_TealBold "\x1b[1;36m"      // Cyan
#define L_CyanBold "\x1b[1;96m"      // Cyan

// Background
#define L_OnBlack "\x1b[40m"    // Black
#define L_OnCoal "\x1b[100m"    // Black
#define L_OnGray "\x1b[47m"     // White
#define L_OnWhite "\x1b[107m"   // White
#define L_OnMaroon "\x1b[41m"   // Red
#define L_OnRed "\x1b[101m"     // Red
#define L_OnGreen "\x1b[42m"    // Green
#define L_OnLime "\x1b[102m"    // Green
#define L_OnOrange "\x1b[43m"   // Yellow
#define L_OnYellow "\x1b[103m"  // Yellow
#define L_OnNavy "\x1b[44m"     // Blue
#define L_OnBlue "\x1b[104m"    // Blue
#define L_OnViolet "\x1b[45m"   // Purple
#define L_OnPurple "\x1b[105m"  // Purple
#define L_OnTeal "\x1b[46m"     // Cyan
#define L_OnCyan "\x1b[106m"    // Cyan

// Underline
#define L_BlackUnder "\x1b[4;30m"       // Black
#define L_GrayUnder "\x1b[4;37m"        // White
#define L_MaroonUnder "\x1b[4;31m"      // Red
#define L_GreenUnder "\x1b[4;32m"       // Green
#define L_OrangeUnder "\x1b[4;33m"      // Yellow
#define L_NavyUnder "\x1b[4;34m"        // Blue
#define L_VioletUnder "\x1b[4;35m"      // Purple
#define L_TealUnder "\x1b[4;36m"        // Cyan


struct DefaultLogger { static const char* name(); static const char* color(); };
struct ErrorLogger: public DefaultLogger { static const char* name(); static const char* color(); };
struct WarningLogger: public DefaultLogger { static const char* name(); static const char* color(); };
struct InfoLogger: public DefaultLogger { static const char* name(); static const char* color(); };
struct DebugLogger: public DefaultLogger { static const char* name(); static const char* color(); };
struct TraceLogger: public DefaultLogger { static const char* name(); static const char* color(); };

class LoggerBase
{
public:
    LoggerBase();
    virtual ~LoggerBase();

protected:
    string getDateTimeString();

    stringstream m_buffer;

private:
    void writeLogToOutput();
    void writeLogToFile();
    
    string m_logFileName;
};

template <class channel>
class Logger : public LoggerBase
{
public:
    Logger()
    {
        m_name = channel::name();
        m_color = channel::color();

        m_buffer << getDateTimeString() << " | " << m_color << m_name << L_Reset << " | ";
    }

    template <class T>
    Logger& operator<<(T const& t) { m_buffer << t; return *this; }

private:
    string m_name;
    string m_color;
};

}} // namespace end

#endif // CPFM_LOG_H_INCLUDED