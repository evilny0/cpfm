/*
 * Copyright (c) 2018, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include <signal.h>
#include "pf.h"

static volatile int stopProgram = 0;
PortfolioManager* portfolioManager = NULL;

void signalHandler(int sig)
{
	stopProgram = 1;
}

int main (int argc, char* argv[])
{
	portfolioManager = new PortfolioManager();

	signal(SIGINT, signalHandler);
	signal(SIGTERM, signalHandler);
	
	portfolioManager->doTestStuff();
	
	/*
    portfolioManager->startThread();

	while (!stopProgram)
	{
#ifdef _WIN32
        SwitchToThread();
        Sleep (1000);
#else
		pthread_yield();
		sleep (1);
#endif
	}

    portfolioManager->stopThread();
	*/

    return 0;
}
