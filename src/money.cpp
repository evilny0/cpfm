/*
 * Copyright (c) 2018, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "log.h"
#include "money.h"

Money::Money ()
{
    m_amount = 0;
}

Money::Money (bmp::mpf_float_50 x)
{
    m_amount = x;
}

Money::Money (int x)
{
    m_amount = x;
}

Money::Money (const std::string& s)
{
    bmp::mpf_float_50 x(s);
    m_amount = x;
}

Money::Money (const web::json::value& x)
{
    if (x.is_string())
    {
        bmp::mpf_float_50 a(x.as_string());
        m_amount = a;
    }
    else if (x.is_integer())
    {
        m_amount = x.as_integer();
    }
    else if (x.is_double())
    {
        // Can't use double, or precision will be lost.
        // And way too much time will be spent wondering why a simple multiplication does not work as expected.
        bmp::mpf_float_50 a(x.serialize());
        m_amount = a;
    }
    else
    {
        lerr << "Unsupported json conversion [" << x << "]. Setting value to 0.";
        m_amount = 0;
    }
}

Money::~Money()
{

}

Money& Money::operator=(const bmp::mpf_float_50& x)
{
    m_amount = x;
    return *this;
}

Money& Money::operator=(const std::string& s)
{
    bmp::mpf_float_50 x(s);
    m_amount = x;
    return *this;
}

Money& Money::operator=(const int& x)
{
    m_amount = x;
    return *this;
}

Money& Money::operator+=(const Money& x)
{
    m_amount+=x.m_amount;
    return *this;
}

Money& Money::operator-=(const Money& x)
{
    m_amount-=x.m_amount;
    return *this;
}

Money& Money::operator*=(const Money& x)
{
    m_amount*=x.m_amount;
    return *this;
}

Money& Money::operator/=(const Money& x)
{
    m_amount/=x.m_amount;
    return *this;
}


bool operator==(const Money& a, const int& b)
{
    if (b == 0)
    {
        return (abs(a.m_amount) < std::numeric_limits<bmp::mpf_float_50>::epsilon());
    }
    else
    {
        return (a.m_amount == b);
    }
}

bool operator<(const Money& a, const int& b)
{
    // First, check if equal. We need to rely on epsilon.
    if (abs(a.m_amount-b) < std::numeric_limits<bmp::mpf_float_50>::epsilon())
        return false;
    
    return (a.m_amount<b);
}

bool operator==(const Money& a, const Money& b)
{
    if (abs(b.m_amount) < std::numeric_limits<bmp::mpf_float_50>::epsilon())
    {
        return (abs(a.m_amount) < std::numeric_limits<bmp::mpf_float_50>::epsilon());
    }
    else
    {
        return (a.m_amount == b.m_amount);
    }
}

bool operator<(const Money& a, const Money& b)
{
    // First, check if equal. We need to rely on epsilon.
    if (abs(a.m_amount-b.m_amount) < std::numeric_limits<bmp::mpf_float_50>::epsilon())
        return false;
    
    return (a.m_amount<b.m_amount);
}

std::ostream& operator<<(std::ostream& os, const Money& obj)
{
    if (obj == 0)
    {
        os << "0";
    }
    else
    {
        stringstream ss;
        ss << std::fixed << std::setprecision(18) << obj.m_amount;
        os << ss.str();
    }

    return os;
}
