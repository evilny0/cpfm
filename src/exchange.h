/*
 * Copyright (c) 2018, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef CPFM_EXCHANGE_H_INCLUDED
#define CPFM_EXCHANGE_H_INCLUDED

#include "pf.h"

#define CPFM_EXCHANGE_ID_KRAKEN 1

#define CPFM_LEDGER_OPERATION_TYPE_DEPOSIT  1
#define CPFM_LEDGER_OPERATION_TYPE_WITHDRAW 2
#define CPFM_LEDGER_OPERATION_TYPE_TRADE    3
#define CPFM_LEDGER_OPERATION_TYPE_TRANSFER 4 // Happens for example on hardforks. Like a deposit, but not initiated by the user.

#define CPFM_TRADE_TYPE_BUY  1
#define CPFM_TRADE_TYPE_SELL 2

#define CPFM_TRADE_ORDER_TYPE_LIMIT  1
#define CPFM_TRADE_ORDER_TYPE_MARKET 2

class ExchangesManager
{
public:
    ExchangesManager(int userId);
    virtual ~ExchangesManager();

    void analyzeUserAccounts();

protected:
    int m_userId;
};

class ExchangeHandler
{
public:
    ExchangeHandler(int userId, int accountId);
    virtual ~ExchangeHandler();

    virtual void analyzeUserData() = 0;

protected:
    void getLedgersTotals();
    void emptyLedgers();
    
    void getTradesTotals();
    void emptyTrades();

    int m_userId;
    int m_accountId;
};

#endif // CPFM_EXCHANGE_H_INCLUDED