/*
 * Copyright (c) 2018, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef CPFM_WALLET_H_INCLUDED
#define CPFM_WALLET_H_INCLUDED

#include "pf.h"

#define CPFM_WALLET_TYPE_ID_BTC 1
#define CPFM_WALLET_TYPE_ID_ETH 2
#define CPFM_WALLET_TYPE_ID_BCH 3

#define CPFM_WALLET_OPERATION_UNKNOWN        0
#define CPFM_WALLET_OPERATION_MINING_MINTED  1
#define CPFM_WALLET_OPERATION_MINING_POOL    2
#define CPFM_WALLET_OPERATION_TRANSFERT      3

#define CPFM_BLOCKCHAIN_ID_BTC 1
#define CPFM_BLOCKCHAIN_ID_BCH 2
#define CPFM_BLOCKCHAIN_ID_ETH 3

string getBlockchainName(int blockchainId);

class WalletsManager
{
public:
    WalletsManager(int userId);
    ~WalletsManager();

    void analyzeUserWallets();

private:
    void emptyWalletsTx();
    
    int m_userId;
};

class Wallet
{
public:
    Wallet (int walletId) { m_walletId = walletId; }

    virtual void update() = 0;

protected:
    int m_walletId;
};

#endif // CPFM_WALLET_H_INCLUDED