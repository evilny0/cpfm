/*
 * Copyright (c) 2018, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef CPFM_SQL_H_INCLUDED
#define CPFM_SQL_H_INCLUDED

#include <sqlpp11/sqlpp11.h>
#include <sqlpp11/mysql/mysql.h>

using namespace sqlpp;

std::shared_ptr<mysql::connection_config> getMysqlConfig();

namespace TableCoins_
{
    struct CoinId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "coin_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T coin_id;
                T& operator()()
                {
                    return coin_id;
                }
                const T& operator()() const
                {
                    return coin_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct CoinName
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "coin_name";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T coin_name;
                T& operator()()
                {
                    return coin_name;
                }
                const T& operator()() const
                {
                    return coin_name;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::varchar>;
    };

    struct CoinShort
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "coin_short";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T coin_short;
                T& operator()()
                {
                    return coin_short;
                }
                const T& operator()() const
                {
                    return coin_short;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::varchar>;
    };
}

struct TableCoins : sqlpp::table_t<TableCoins,TableCoins_::CoinId,TableCoins_::CoinName,TableCoins_::CoinShort>
{
    using _value_type = sqlpp::no_value_t;
    struct _alias_t
    {
        static constexpr const char _literal[] = "coins";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template <typename T>
        struct _member_t
        {
            T coins;
            T& operator()()
            {
                return coins;
            }
            const T& operator()() const
            {
                return coins;
            }
        };
    };
};

namespace TableWallets_
{
    struct WalletId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "wallet_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T wallet_id;
                T& operator()()
                {
                    return wallet_id;
                }
                const T& operator()() const
                {
                    return wallet_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct UserId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "user_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T user_id;
                T& operator()()
                {
                    return user_id;
                }
                const T& operator()() const
                {
                    return user_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct TypeId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "type_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T type_id;
                T& operator()()
                {
                    return type_id;
                }
                const T& operator()() const
                {
                    return type_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct WalletName
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "wallet_name";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T wallet_name;
                T& operator()()
                {
                    return wallet_name;
                }
                const T& operator()() const
                {
                    return wallet_name;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::varchar>;
    };

    struct Active
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "active";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T active;
                T& operator()()
                {
                    return active;
                }
                const T& operator()() const
                {
                    return active;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::boolean>;
    };
}

struct TableWallets : sqlpp::table_t<TableWallets,TableWallets_::WalletId,TableWallets_::UserId,TableWallets_::TypeId,TableWallets_::WalletName,TableWallets_::Active>
{
    using _value_type = sqlpp::no_value_t;
    struct _alias_t
    {
        static constexpr const char _literal[] = "wallets";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template <typename T>
        struct _member_t
        {
            T wallets;
            T& operator()()
            {
                return wallets;
            }
            const T& operator()() const
            {
                return wallets;
            }
        };
    };
};

namespace TableWalletsAddresses_
{
    struct AddressId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "address_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T address_id;
                T& operator()()
                {
                    return address_id;
                }
                const T& operator()() const
                {
                    return address_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct WalletId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "wallet_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T wallet_id;
                T& operator()()
                {
                    return wallet_id;
                }
                const T& operator()() const
                {
                    return wallet_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct Address
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "address";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T address;
                T& operator()()
                {
                    return address;
                }
                const T& operator()() const
                {
                    return address;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::varchar>;
    };

    struct Comment
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "comment";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T comment;
                T& operator()()
                {
                    return comment;
                }
                const T& operator()() const
                {
                    return comment;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::varchar>;
    };

    struct IsChange
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "is_change";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T is_change;
                T& operator()()
                {
                    return is_change;
                }
                const T& operator()() const
                {
                    return is_change;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::boolean>;
    };
}

struct TableWalletsAddresses : sqlpp::table_t<TableWalletsAddresses,TableWalletsAddresses_::AddressId,TableWalletsAddresses_::WalletId,TableWalletsAddresses_::Address,TableWalletsAddresses_::Comment,TableWalletsAddresses_::IsChange>
{
    using _value_type = sqlpp::no_value_t;
    struct _alias_t
    {
        static constexpr const char _literal[] = "wallets_addresses";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template <typename T>
        struct _member_t
        {
            T wallets_addresses;
            T& operator()()
            {
                return wallets_addresses;
            }
            const T& operator()() const
            {
                return wallets_addresses;
            }
        };
    };
};

namespace TableWalletsTx_
{
    struct TxId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "tx_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T tx_id;
                T& operator()()
                {
                    return tx_id;
                }
                const T& operator()() const
                {
                    return tx_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct WalletId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "wallet_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T wallet_id;
                T& operator()()
                {
                    return wallet_id;
                }
                const T& operator()() const
                {
                    return wallet_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct Amount
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "amount";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T amount;
                T& operator()()
                {
                    return amount;
                }
                const T& operator()() const
                {
                    return amount;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::decimal>;
    };

    struct Fee
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "fee";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T fee;
                T& operator()()
                {
                    return fee;
                }
                const T& operator()() const
                {
                    return fee;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::decimal>;
    };

    struct AmountCoinId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "amount_coin_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T amount_coin_id;
                T& operator()()
                {
                    return amount_coin_id;
                }
                const T& operator()() const
                {
                    return amount_coin_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct FeeCoinId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "fee_coin_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T fee_coin_id;
                T& operator()()
                {
                    return fee_coin_id;
                }
                const T& operator()() const
                {
                    return fee_coin_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct UnixTime
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "unix_time";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T unix_time;
                T& operator()()
                {
                    return unix_time;
                }
                const T& operator()() const
                {
                    return unix_time;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct OperationType
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "operation_type";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T operation_type;
                T& operator()()
                {
                    return operation_type;
                }
                const T& operator()() const
                {
                    return operation_type;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };
}

struct TableWalletsTx : sqlpp::table_t<TableWalletsTx,TableWalletsTx_::TxId,TableWalletsTx_::WalletId,TableWalletsTx_::Amount,TableWalletsTx_::Fee,TableWalletsTx_::AmountCoinId,TableWalletsTx_::FeeCoinId,TableWalletsTx_::UnixTime,TableWalletsTx_::OperationType>
{
    using _value_type = sqlpp::no_value_t;
    struct _alias_t
    {
        static constexpr const char _literal[] = "wallets_tx";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template <typename T>
        struct _member_t
        {
            T wallets_tx;
            T& operator()()
            {
                return wallets_tx;
            }
            const T& operator()() const
            {
                return wallets_tx;
            }
        };
    };
};

namespace TableWalletsBtcRawTx_
{
    struct RawTxId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "raw_tx_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T raw_tx_id;
                T& operator()()
                {
                    return raw_tx_id;
                }
                const T& operator()() const
                {
                    return raw_tx_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct WalletId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "wallet_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T wallet_id;
                T& operator()()
                {
                    return wallet_id;
                }
                const T& operator()() const
                {
                    return wallet_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };
}

struct TableWalletsBtcRawTx : sqlpp::table_t<TableWalletsBtcRawTx,TableWalletsBtcRawTx_::RawTxId,TableWalletsBtcRawTx_::WalletId>
{
    using _value_type = sqlpp::no_value_t;
    struct _alias_t
    {
        static constexpr const char _literal[] = "wallets_btc_raw_tx";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template <typename T>
        struct _member_t
        {
            T wallets_btc_raw_tx;
            T& operator()()
            {
                return wallets_btc_raw_tx;
            }
            const T& operator()() const
            {
                return wallets_btc_raw_tx;
            }
        };
    };
};

namespace TableWalletsEthRawTx_
{
    struct RawTxId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "raw_tx_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T raw_tx_id;
                T& operator()()
                {
                    return raw_tx_id;
                }
                const T& operator()() const
                {
                    return raw_tx_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct WalletId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "wallet_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T wallet_id;
                T& operator()()
                {
                    return wallet_id;
                }
                const T& operator()() const
                {
                    return wallet_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };
}

struct TableWalletsEthRawTx : sqlpp::table_t<TableWalletsEthRawTx,TableWalletsEthRawTx_::RawTxId,TableWalletsEthRawTx_::WalletId>
{
    using _value_type = sqlpp::no_value_t;
    struct _alias_t
    {
        static constexpr const char _literal[] = "wallets_eth_raw_tx";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template <typename T>
        struct _member_t
        {
            T wallets_eth_raw_tx;
            T& operator()()
            {
                return wallets_eth_raw_tx;
            }
            const T& operator()() const
            {
                return wallets_eth_raw_tx;
            }
        };
    };
};

namespace TableBlockchainBtcRawTx_
{
    struct RawTxId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "raw_tx_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T raw_tx_id;
                T& operator()()
                {
                    return raw_tx_id;
                }
                const T& operator()() const
                {
                    return raw_tx_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct BlockchainId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "blockchain_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T blockchain_id;
                T& operator()()
                {
                    return blockchain_id;
                }
                const T& operator()() const
                {
                    return blockchain_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct Hash
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "hash";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T hash;
                T& operator()()
                {
                    return hash;
                }
                const T& operator()() const
                {
                    return hash;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::varchar>;
    };

    struct UnixTime
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "unix_time";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T unix_time;
                T& operator()()
                {
                    return unix_time;
                }
                const T& operator()() const
                {
                    return unix_time;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };
}

struct TableBlockchainBtcRawTx : sqlpp::table_t<TableBlockchainBtcRawTx,TableBlockchainBtcRawTx_::RawTxId,TableBlockchainBtcRawTx_::BlockchainId,TableBlockchainBtcRawTx_::Hash,TableBlockchainBtcRawTx_::UnixTime>
{
    using _value_type = sqlpp::no_value_t;
    struct _alias_t
    {
        static constexpr const char _literal[] = "blockchain_btc_raw_tx";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template <typename T>
        struct _member_t
        {
            T blockchain_btc_raw_tx;
            T& operator()()
            {
                return blockchain_btc_raw_tx;
            }
            const T& operator()() const
            {
                return blockchain_btc_raw_tx;
            }
        };
    };
};

namespace TableBlockchainBtcRawTxDetails_
{
    struct RawTxDetailId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "raw_tx_detail_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T raw_tx_detail_id;
                T& operator()()
                {
                    return raw_tx_detail_id;
                }
                const T& operator()() const
                {
                    return raw_tx_detail_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct RawTxId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "raw_tx_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T raw_tx_id;
                T& operator()()
                {
                    return raw_tx_id;
                }
                const T& operator()() const
                {
                    return raw_tx_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct Amount
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "amount";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T amount;
                T& operator()()
                {
                    return amount;
                }
                const T& operator()() const
                {
                    return amount;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::decimal>;
    };

    struct Address
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "address";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T address;
                T& operator()()
                {
                    return address;
                }
                const T& operator()() const
                {
                    return address;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::varchar>;
    };
}

struct TableBlockchainBtcRawTxDetails : sqlpp::table_t<TableBlockchainBtcRawTxDetails,TableBlockchainBtcRawTxDetails_::RawTxDetailId,TableBlockchainBtcRawTxDetails_::RawTxId,TableBlockchainBtcRawTxDetails_::Amount,TableBlockchainBtcRawTxDetails_::Address>
{
    using _value_type = sqlpp::no_value_t;
    struct _alias_t
    {
        static constexpr const char _literal[] = "blockchain_btc_raw_tx_details";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template <typename T>
        struct _member_t
        {
            T blockchain_btc_raw_tx_details;
            T& operator()()
            {
                return blockchain_btc_raw_tx_details;
            }
            const T& operator()() const
            {
                return blockchain_btc_raw_tx_details;
            }
        };
    };
};

namespace TableBlockchainEthRawTx_
{
    struct RawTxId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "raw_tx_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T raw_tx_id;
                T& operator()()
                {
                    return raw_tx_id;
                }
                const T& operator()() const
                {
                    return raw_tx_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct BlockchainId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "blockchain_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T blockchain_id;
                T& operator()()
                {
                    return blockchain_id;
                }
                const T& operator()() const
                {
                    return blockchain_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct Hash
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "hash";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T hash;
                T& operator()()
                {
                    return hash;
                }
                const T& operator()() const
                {
                    return hash;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::varchar>;
    };

    struct UnixTime
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "unix_time";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T unix_time;
                T& operator()()
                {
                    return unix_time;
                }
                const T& operator()() const
                {
                    return unix_time;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct Fee
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "fee";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T fee;
                T& operator()()
                {
                    return fee;
                }
                const T& operator()() const
                {
                    return fee;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::decimal>;
    };
}

struct TableBlockchainEthRawTx : sqlpp::table_t<TableBlockchainEthRawTx,TableBlockchainEthRawTx_::RawTxId,TableBlockchainEthRawTx_::BlockchainId,TableBlockchainEthRawTx_::Hash,TableBlockchainEthRawTx_::UnixTime,TableBlockchainEthRawTx_::Fee>
{
    using _value_type = sqlpp::no_value_t;
    struct _alias_t
    {
        static constexpr const char _literal[] = "blockchain_eth_raw_tx";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template <typename T>
        struct _member_t
        {
            T blockchain_eth_raw_tx;
            T& operator()()
            {
                return blockchain_eth_raw_tx;
            }
            const T& operator()() const
            {
                return blockchain_eth_raw_tx;
            }
        };
    };
};

namespace TableBlockchainEthRawTxDetails_
{
    struct RawTxDetailId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "raw_tx_detail_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T raw_tx_detail_id;
                T& operator()()
                {
                    return raw_tx_detail_id;
                }
                const T& operator()() const
                {
                    return raw_tx_detail_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct RawTxId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "raw_tx_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T raw_tx_id;
                T& operator()()
                {
                    return raw_tx_id;
                }
                const T& operator()() const
                {
                    return raw_tx_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct AddressFrom
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "address_from";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T address_from;
                T& operator()()
                {
                    return address_from;
                }
                const T& operator()() const
                {
                    return address_from;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::varchar>;
    };

    struct AddressTo
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "address_to";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T address_to;
                T& operator()()
                {
                    return address_to;
                }
                const T& operator()() const
                {
                    return address_to;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::varchar>;
    };

    struct AmountCoinId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "amount_coin_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T amount_coin_id;
                T& operator()()
                {
                    return amount_coin_id;
                }
                const T& operator()() const
                {
                    return amount_coin_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct Amount
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "amount";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T amount;
                T& operator()()
                {
                    return amount;
                }
                const T& operator()() const
                {
                    return amount;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::decimal>;
    };
};

struct TableBlockchainEthRawTxDetails : sqlpp::table_t<TableBlockchainEthRawTxDetails,TableBlockchainEthRawTxDetails_::RawTxDetailId,TableBlockchainEthRawTxDetails_::RawTxId,TableBlockchainEthRawTxDetails_::AddressFrom,TableBlockchainEthRawTxDetails_::AddressTo,TableBlockchainEthRawTxDetails_::AmountCoinId,TableBlockchainEthRawTxDetails_::Amount>
{
    using _value_type = sqlpp::no_value_t;
    struct _alias_t
    {
        static constexpr const char _literal[] = "blockchain_eth_raw_tx_details";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template <typename T>
        struct _member_t
        {
            T blockchain_eth_raw_tx_details;
            T& operator()()
            {
                return blockchain_eth_raw_tx_details;
            }
            const T& operator()() const
            {
                return blockchain_eth_raw_tx_details;
            }
        };
    };
};

namespace TableExchangesLedgers_
{
    struct LedgerId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "ledger_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T ledger_id;
                T& operator()()
                {
                    return ledger_id;
                }
                const T& operator()() const
                {
                    return ledger_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct CoinId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "coin_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T coin_id;
                T& operator()()
                {
                    return coin_id;
                }
                const T& operator()() const
                {
                    return coin_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct AccountId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "account_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T account_id;
                T& operator()()
                {
                    return account_id;
                }
                const T& operator()() const
                {
                    return account_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct ExchangeLedgerId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "exchange_ledger_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T exchange_ledger_id;
                T& operator()()
                {
                    return exchange_ledger_id;
                }
                const T& operator()() const
                {
                    return exchange_ledger_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::varchar>;
    };

    struct ExchangeReferenceId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "exchange_reference_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T exchange_reference_id;
                T& operator()()
                {
                    return exchange_reference_id;
                }
                const T& operator()() const
                {
                    return exchange_reference_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::varchar>;
    };

    struct UnixTime
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "unix_time";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T unix_time;
                T& operator()()
                {
                    return unix_time;
                }
                const T& operator()() const
                {
                    return unix_time;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct OperationType
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "operation_type";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T operation_type;
                T& operator()()
                {
                    return operation_type;
                }
                const T& operator()() const
                {
                    return operation_type;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct Amount
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "amount";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T amount;
                T& operator()()
                {
                    return amount;
                }
                const T& operator()() const
                {
                    return amount;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::decimal>;
    };

    struct Fee
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "fee";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T fee;
                T& operator()()
                {
                    return fee;
                }
                const T& operator()() const
                {
                    return fee;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::decimal>;
    };
}

struct TableExchangesLedgers : sqlpp::table_t<TableExchangesLedgers,TableExchangesLedgers_::LedgerId,TableExchangesLedgers_::AccountId,TableExchangesLedgers_::ExchangeLedgerId,TableExchangesLedgers_::ExchangeReferenceId,TableExchangesLedgers_::UnixTime,TableExchangesLedgers_::OperationType,TableExchangesLedgers_::CoinId,TableExchangesLedgers_::Amount,TableExchangesLedgers_::Fee>
{
    using _value_type = sqlpp::no_value_t;
    struct _alias_t
    {
        static constexpr const char _literal[] = "exchanges_ledgers";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template <typename T>
        struct _member_t
        {
            T exchanges_ledgers;
            T& operator()()
            {
                return exchanges_ledgers;
            }
            const T& operator()() const
            {
                return exchanges_ledgers;
            }
        };
    };
};

namespace TableExchangesTrades_
{
    struct TradeId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "trade_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T trade_id;
                T& operator()()
                {
                    return trade_id;
                }
                const T& operator()() const
                {
                    return trade_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct AccountId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "account_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T account_id;
                T& operator()()
                {
                    return account_id;
                }
                const T& operator()() const
                {
                    return account_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct ExchangeTradeId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "exchange_trade_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T exchange_trade_id;
                T& operator()()
                {
                    return exchange_trade_id;
                }
                const T& operator()() const
                {
                    return exchange_trade_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::varchar>;
    };

    struct ExchangeOrderId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "exchange_order_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T exchange_order_id;
                T& operator()()
                {
                    return exchange_order_id;
                }
                const T& operator()() const
                {
                    return exchange_order_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::varchar>;
    };

    struct BaseCoinId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "base_coin_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T base_coin_id;
                T& operator()()
                {
                    return base_coin_id;
                }
                const T& operator()() const
                {
                    return base_coin_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct QuoteCoinId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "quote_coin_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T quote_coin_id;
                T& operator()()
                {
                    return quote_coin_id;
                }
                const T& operator()() const
                {
                    return quote_coin_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct UnixTime
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "unix_time";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T unix_time;
                T& operator()()
                {
                    return unix_time;
                }
                const T& operator()() const
                {
                    return unix_time;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct Type
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "type";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T type;
                T& operator()()
                {
                    return type;
                }
                const T& operator()() const
                {
                    return type;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct OrderType
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "order_type";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T order_type;
                T& operator()()
                {
                    return order_type;
                }
                const T& operator()() const
                {
                    return order_type;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct Price
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "price";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T price;
                T& operator()()
                {
                    return price;
                }
                const T& operator()() const
                {
                    return price;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::decimal>;
    };

    struct Cost
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "cost";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T cost;
                T& operator()()
                {
                    return cost;
                }
                const T& operator()() const
                {
                    return cost;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::decimal>;
    };

    struct Fee
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "fee";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T fee;
                T& operator()()
                {
                    return fee;
                }
                const T& operator()() const
                {
                    return fee;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::decimal>;
    };

    struct FeeCoinId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "fee_coin_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T fee_coin_id;
                T& operator()()
                {
                    return fee_coin_id;
                }
                const T& operator()() const
                {
                    return fee_coin_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct Volume
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "volume";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T volume;
                T& operator()()
                {
                    return volume;
                }
                const T& operator()() const
                {
                    return volume;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::decimal>;
    };
}

struct TableExchangesTrades : sqlpp::table_t<TableExchangesTrades,TableExchangesTrades_::TradeId,TableExchangesTrades_::AccountId,TableExchangesTrades_::ExchangeTradeId,TableExchangesTrades_::ExchangeOrderId,TableExchangesTrades_::BaseCoinId,TableExchangesTrades_::QuoteCoinId,TableExchangesTrades_::UnixTime,TableExchangesTrades_::Type,TableExchangesTrades_::OrderType,TableExchangesTrades_::Price,TableExchangesTrades_::Cost,TableExchangesTrades_::Fee,TableExchangesTrades_::Volume,TableExchangesTrades_::FeeCoinId>
{
    using _value_type = sqlpp::no_value_t;
    struct _alias_t
    {
        static constexpr const char _literal[] = "exchanges_trades";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template <typename T>
        struct _member_t
        {
            T exchanges_trades;
            T& operator()()
            {
                return exchanges_trades;
            }
            const T& operator()() const
            {
                return exchanges_trades;
            }
        };
    };
};

namespace TableExchangesAccounts_
{
    struct AccountId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "account_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T account_id;
                T& operator()()
                {
                    return account_id;
                }
                const T& operator()() const
                {
                    return account_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct UserId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "user_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T user_id;
                T& operator()()
                {
                    return user_id;
                }
                const T& operator()() const
                {
                    return user_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct ExchangeId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "exchange_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T exchange_id;
                T& operator()()
                {
                    return exchange_id;
                }
                const T& operator()() const
                {
                    return exchange_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct ApiKey
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "api_key";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T api_key;
                T& operator()()
                {
                    return api_key;
                }
                const T& operator()() const
                {
                    return api_key;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::varchar>;
    };

    struct ApiPrivateKey
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "api_private_key";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T api_private_key;
                T& operator()()
                {
                    return api_private_key;
                }
                const T& operator()() const
                {
                    return api_private_key;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::varchar>;
    };
}

struct TableExchangesAccounts : sqlpp::table_t<TableExchangesAccounts,TableExchangesAccounts_::AccountId,TableExchangesAccounts_::UserId,TableExchangesAccounts_::ExchangeId,TableExchangesAccounts_::ApiKey,TableExchangesAccounts_::ApiPrivateKey>
{
    using _value_type = sqlpp::no_value_t;
    struct _alias_t
    {
        static constexpr const char _literal[] = "exchanges_accounts";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template <typename T>
        struct _member_t
        {
            T exchanges_accounts;
            T& operator()()
            {
                return exchanges_accounts;
            }
            const T& operator()() const
            {
                return exchanges_accounts;
            }
        };
    };
};

namespace TableWalletsBalances_
{
    struct WalletId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "wallet_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T wallet_id;
                T& operator()()
                {
                    return wallet_id;
                }
                const T& operator()() const
                {
                    return wallet_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct CoinId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "coin_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T coin_id;
                T& operator()()
                {
                    return coin_id;
                }
                const T& operator()() const
                {
                    return coin_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct Balance
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "balance";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T balance;
                T& operator()()
                {
                    return balance;
                }
                const T& operator()() const
                {
                    return balance;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::decimal>;
    };
}

struct TableWalletsBalances : sqlpp::table_t<TableWalletsBalances,TableWalletsBalances_::WalletId,TableWalletsBalances_::CoinId,TableWalletsBalances_::Balance>
{
    using _value_type = sqlpp::no_value_t;
    struct _alias_t
    {
        static constexpr const char _literal[] = "wallets_balances";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template <typename T>
        struct _member_t
        {
            T wallets_balances;
            T& operator()()
            {
                return wallets_balances;
            }
            const T& operator()() const
            {
                return wallets_balances;
            }
        };
    };
};

namespace TableExchangesBalances_
{
    struct AccountId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "account_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T account_id;
                T& operator()()
                {
                    return account_id;
                }
                const T& operator()() const
                {
                    return account_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct CoinId
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "coin_id";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T coin_id;
                T& operator()()
                {
                    return coin_id;
                }
                const T& operator()() const
                {
                    return coin_id;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };

    struct Balance
    {
        struct _alias_t
        {
            static constexpr const char _literal[] = "balance";
            using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
            template <typename T>
            struct _member_t
            {
                T balance;
                T& operator()()
                {
                    return balance;
                }
                const T& operator()() const
                {
                    return balance;
                }
            };
        };
        using _traits = sqlpp::make_traits<sqlpp::decimal>;
    };
}

struct TableExchangesBalances : sqlpp::table_t<TableExchangesBalances,TableExchangesBalances_::AccountId,TableExchangesBalances_::CoinId,TableExchangesBalances_::Balance>
{
    using _value_type = sqlpp::no_value_t;
    struct _alias_t
    {
        static constexpr const char _literal[] = "exchanges_balances";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template <typename T>
        struct _member_t
        {
            T exchanges_balances;
            T& operator()()
            {
                return exchanges_balances;
            }
            const T& operator()() const
            {
                return exchanges_balances;
            }
        };
    };
};

#endif // CPFM_SQL_H_INCLUDED