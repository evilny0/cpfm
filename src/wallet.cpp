/*
 * Copyright (c) 2018, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "wallet.h"
#include "wallets/wallet_btc.h"
#include "wallets/wallet_bch.h"
#include "wallets/wallet_eth.h"

string getBlockchainName(int blockchainId)
{
    string s;

    switch(blockchainId)
    {
        case CPFM_BLOCKCHAIN_ID_BTC:
            s = "BTC";
            break;
        case CPFM_BLOCKCHAIN_ID_BCH:
            s = "BCH";
            break;
        case CPFM_BLOCKCHAIN_ID_ETH:
            s = "ETH";
            break;
        default:
            s = "Error!";
            break;
    }

    return s;   
}

WalletsManager::WalletsManager(int userId)
{
    m_userId = userId;
}

WalletsManager::~WalletsManager()
{

}

void WalletsManager::analyzeUserWallets()
{
    mysql::connection db(getMysqlConfig());
    const auto wallets = TableWallets{};

    list<Wallet*> userWallets;
    for (const auto& row: db.run(select(wallets.type_id,wallets.wallet_id).from(wallets).where(wallets.user_id == m_userId and wallets.active == true)))
    {
        Wallet* wallet;
        switch (row.type_id)
        {
        case CPFM_WALLET_TYPE_ID_BTC:
            wallet = new WalletBTC(row.wallet_id);
            userWallets.push_back(wallet);
            break;
        case CPFM_WALLET_TYPE_ID_BCH:
            wallet = new WalletBCH(row.wallet_id);
            userWallets.push_back(wallet);
            break;
        case CPFM_WALLET_TYPE_ID_ETH:
            wallet = new WalletETH(row.wallet_id);
            userWallets.push_back(wallet);
            break;
        default:
            lerr << "Unsupported wallet type: " << row.type_id;
            break;
        }
    }

    for (auto const& wallet: userWallets)
    {
        wallet->update();
        delete wallet;
    }
}

void WalletsManager::emptyWalletsTx()
{
    mysql::connection db(getMysqlConfig());
    const auto wallets = TableWallets{};
    const auto wallets_tx = TableWalletsTx{};

    list<int> userWallets;
    for (const auto& row: db.run(select(wallets.wallet_id).from(wallets).where(wallets.user_id == m_userId)))
    {
        userWallets.push_back(row.wallet_id);
    }

    for (const auto& idWallet: userWallets)
    {
        list<int> userWalletTx;
        for (const auto& row: db.run(select(wallets_tx.tx_id).from(wallets_tx).where(wallets_tx.wallet_id == idWallet)))
        {
            userWalletTx.push_back(row.tx_id);
        }

        db.run(remove_from(wallets_tx).where(wallets_tx.wallet_id == idWallet));
    }
    
    linfo << "Emptied Wallets Tx DB table for user " << m_userId;
}
