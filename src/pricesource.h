/*
 * Copyright (c) 2018, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef CPFM_PRICESOURCE_H_INCLUDED
#define CPFM_PRICESOURCE_H_INCLUDED

#include "pf.h"

class PriceSource
{
public:
    PriceSource();
    ~PriceSource();

    Money getCoinPrice(int coinId);

protected:
    virtual void getData() = 0;
    virtual Money getCoinPriceFromData(int coinId) = 0;
};

class PriceSourceCryptoWatch : public PriceSource
{
public:
    PriceSourceCryptoWatch();
    ~PriceSourceCryptoWatch();

protected:
    virtual void getData();
    virtual Money getCoinPriceFromData(int coinId);
    string getCacheFilename();
};

#endif // CPFM_PRICESOURCE_H_INCLUDED