/*
 * Copyright (c) 2021, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef CPFM_DATASOURCE_ETHERSCAN_H_INCLUDED
#define CPFM_DATASOURCE_ETHERSCAN_H_INCLUDED

#include "datasources/datasource.h"


class BlockchainDataSourceETH_Etherscan
{
public:
    list<BlockchainTxDetailsTypeETH> getTxDetailsListForAddress(string address);

private:
    list<BlockchainTxDetailsTypeETH> getTxDetailsListForAddressTxType(string address, string type, list<string>& hashList);
    void saveBlockchainAddressDataToCacheFile(string address, string type);
    string getCacheFilenameForAddress(string address, string type);
    bool retrieveCacheFilesForAddress(string address);
    int GetCoinIdFromContractAddress(string contractAddress);

    string m_currentRequestCacheFilename;
};

#endif // CPFM_DATASOURCE_ETHERSCAN_H_INCLUDED