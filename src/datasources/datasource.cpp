/*
 * Copyright (c) 2021, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "datasources/datasource.h"
#include "datasources/blockchain_info.h"
#include "datasources/blockchair.h"
#include "datasources/etherscan.h"

list<BlockchainTxDetailsTypeBTC> BlockchainDataSourceBTC::getTxDetailsListForAddresses(list<string> addresses)
{
    list<BlockchainTxDetailsTypeBTC> l;

    // Blockchain.info
    /*
    BlockchainDataSourceBTC_BlockchainInfo bci;
    list<string> hashList;

    for (auto const address: addresses)
    {
        list<BlockchainTxDetailsTypeBTC> listForSingleAddress = bci.getTxDetailsListForAddress(address);

        for (auto const tx: listForSingleAddress)
        {
            if (std::find(hashList.begin(), hashList.end(), tx.hash) == hashList.end())
            {
                hashList.push_back(tx.hash);
                l.push_back(tx);
            }
        }
    }
    */

    // Blockchair
    BlockchainDataSourceBTC_Blockchair bc;
    l = bc.getTxDetailsListForAddresses(addresses);

    return l;
}

list<BlockchainTxDetailsTypeBTC> BlockchainDataSourceBCH::getTxDetailsListForAddresses(list<string> addresses)
{
    list<BlockchainTxDetailsTypeBTC> l;

    // Blockchair
    BlockchainDataSourceBCH_Blockchair bc;
    l = bc.getTxDetailsListForAddresses(addresses);

    return l;
}

list<BlockchainTxDetailsTypeETH> BlockchainDataSourceETH::getTxDetailsListForAddress(string address)
{
    list<BlockchainTxDetailsTypeETH> l;

    // Etherscan
    BlockchainDataSourceETH_Etherscan bc;
    l = bc.getTxDetailsListForAddress(address);

    return l;
}
