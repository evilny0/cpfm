/*
 * Copyright (c) 2021, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef CPFM_DATASOURCE_BLOCKCHAIN_INFO_H_INCLUDED
#define CPFM_DATASOURCE_BLOCKCHAIN_INFO_H_INCLUDED

#include "datasources/datasource.h"


class BlockchainDataSourceBTC_BlockchainInfo
{
public:
    list<BlockchainTxDetailsTypeBTC> getTxDetailsListForAddress(string address);

private:
    string getCacheFilenameForAddress(string address);
    void saveBlockchainAddressDataToCacheFile(string address);

    string m_currentRequestCacheFilename;
};

#endif // CPFM_DATASOURCE_BLOCKCHAIN_INFO_H_INCLUDED