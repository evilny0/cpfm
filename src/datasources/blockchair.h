/*
 * Copyright (c) 2021, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef CPFM_DATASOURCE_BLOCKCHAIR_H_INCLUDED
#define CPFM_DATASOURCE_BLOCKCHAIR_H_INCLUDED

#include "datasources/datasource.h"


class BlockchainDataSourceTypeBTC_Blockchair
{
public:
    list<BlockchainTxDetailsTypeBTC> getTxDetailsListForAddresses(list<string> addresses);

private:
    string getCacheFilenameForAddresses(list<string> address);
    string getCacheFilenameForTx(string txHash);

    void saveBlockchainAddressesDataToCacheFile(list<string> address);
    void saveBlockchainTxDataToCacheFile(string txHash);

    Time getTimeFromString(string s);

    string m_currentRequestCacheFilename;

protected:
    string m_blockchainName;

    virtual void dummyMakeAbstract() = 0;
};

class BlockchainDataSourceBTC_Blockchair : public BlockchainDataSourceTypeBTC_Blockchair
{
public:
    BlockchainDataSourceBTC_Blockchair() { m_blockchainName = "bitcoin"; }

protected:
    virtual void dummyMakeAbstract(){};
};

class BlockchainDataSourceBCH_Blockchair : public BlockchainDataSourceTypeBTC_Blockchair
{
public:
    BlockchainDataSourceBCH_Blockchair() { m_blockchainName = "bitcoin-cash"; }

protected:
    virtual void dummyMakeAbstract(){};
};

#endif // CPFM_DATASOURCE_BLOCKCHAIR_H_INCLUDED