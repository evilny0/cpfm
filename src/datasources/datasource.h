/*
 * Copyright (c) 2021, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef CPFM_DATASOURCE_H_INCLUDED
#define CPFM_DATASOURCE_H_INCLUDED

#include "pf.h"

class BlockchainTxDetailsTypeBTC
{
public:
    string hash;
    Time time;
    map <string,Money> inputs;
    map <string,Money> outputs;
};

class BlockchainTxOperationTypeETH
{
public:
    string addressFrom;
    string addressTo;
    Money amount;
    int amountCoinId;
};

class BlockchainTxDetailsTypeETH
{
public:
    string hash;
    Time time;
    list<BlockchainTxOperationTypeETH> operations;
    Money fee;
};

class BlockchainDataSourceBTC
{
public:
    list<BlockchainTxDetailsTypeBTC> getTxDetailsListForAddresses(list<string> addresses);
};

class BlockchainDataSourceBCH
{
public:
    list<BlockchainTxDetailsTypeBTC> getTxDetailsListForAddresses(list<string> addresses);
};

class BlockchainDataSourceETH
{
public:
    list<BlockchainTxDetailsTypeETH> getTxDetailsListForAddress(string address);
};

#endif // CPFM_DATASOURCE_H_INCLUDED