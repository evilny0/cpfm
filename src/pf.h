/*
 * Copyright (c) 2018, evilny0
 * 
 * This file is part of cpfm.
 *
 * cpfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with cpfm. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef CPFM_CPFM_H_INCLUDED
#define CPFM_CPFM_H_INCLUDED

#include <unistd.h>
#include <fstream>
#include <vector>
#include <list>

#include <boost/multiprecision/gmp.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/filesystem.hpp>

#include <cpprest/http_client.h>
#include <cpprest/json.h>

#include "run.h"
#include "sql.h"
#include "log.h"
#include "money.h"

#define CPFM_COIN_ID_BTC  1
#define CPFM_COIN_ID_LTC  2
#define CPFM_COIN_ID_EUR  3
#define CPFM_COIN_ID_ICN  4
#define CPFM_COIN_ID_ETC  5
#define CPFM_COIN_ID_ETH  6
#define CPFM_COIN_ID_BCH  7
#define CPFM_COIN_ID_BTG  8
#define CPFM_COIN_ID_KNC  9
#define CPFM_COIN_ID_AIR  10
#define CPFM_COIN_ID_LEND 11
#define CPFM_COIN_ID_AAVE 12
#define CPFM_COIN_ID_LPT  13
#define CPFM_COIN_ID_BCDT 14

class Time
{
public:
    void setFromUnixTime(time_t unixTime) { m_unixTime = unixTime; }
    const time_t toUnixTime() const { return m_unixTime; }

private:
    time_t m_unixTime;
    //date::sys_time<std::chrono::milliseconds>
};

namespace bmp = boost::multiprecision;
namespace bfs = boost::filesystem;
using namespace std;
using namespace web;
using namespace web::http;
using namespace web::http::client;
using namespace boost::posix_time;

class PortfolioManager : public Runnable
{
public:
    PortfolioManager();
    virtual ~PortfolioManager();

    void doTestStuff();
    void emptyUserBalances(int userId);
    void displayUserBalances(int userId);
    
protected:
    virtual void run();

private:
};

string getCoinName(int coinId);

#endif // CPFM_CPFM_H_INCLUDED