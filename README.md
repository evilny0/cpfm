# cpfm

Cryptocurrencies portfolio manager.

# Requirements

* MySQL
* C++ REST SDK : https://github.com/Microsoft/cpprestsdk
* OpenSSL (1.1.0+)
* GMP
* Boost
* sqlpp11 (modified to handle decimal type) : https://github.com/evilny0/sqlpp11/tree/decimal
* sqlpp11-connector-mysql (modified to handle decimal type) : https://github.com/evilny0/sqlpp11-connector-mysql/tree/decimal
* Howard Hinnant date lib (needed by sqlpp11) : https://github.com/HowardHinnant/date

# Build

Inside the directory run the following commands:

```bash
mkdir build
cd build
cmake ..
make
```

You should pass the following variables to cmake :

* SQLPP11_ROOT_DIR : where sqlpp11 is installed.
* SQLPP11_MYSQL_INCLUDE_DIR : where sqlpp11-connector-mysql include folder is.
* SQLPP11_MYSQL_LIB_DIR : where sqlpp11-connector-mysql compiled library is.
* HinnantDate_ROOT_DIR : where Howard Hinnant Date is installed.

# Database

You should create a database with the SQL file provided.

# License

* cpfm is distributed under the GNU GPL v3.

* The Kraken signature code is copied from Marco Esposito krakenapi <https://github.com/voidloop/krakenapi>, which is licensed under the MIT license.
