SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


CREATE TABLE `blockchain_btc_raw_tx` (
  `raw_tx_id` int(11) NOT NULL,
  `blockchain_id` int(11) NOT NULL,
  `hash` varchar(200) NOT NULL,
  `unix_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `blockchain_btc_raw_tx_details` (
  `raw_tx_detail_id` int(11) NOT NULL,
  `raw_tx_id` int(11) NOT NULL,
  `amount` decimal(40,18) NOT NULL,
  `address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `blockchain_eth_raw_tx` (
  `raw_tx_id` int(11) NOT NULL,
  `blockchain_id` int(11) NOT NULL,
  `hash` varchar(200) NOT NULL,
  `unix_time` int(11) NOT NULL,
  `fee` decimal(40,18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `blockchain_eth_raw_tx_details` (
  `raw_tx_detail_id` int(11) NOT NULL,
  `raw_tx_id` int(11) NOT NULL,
  `address_from` varchar(200) NOT NULL,
  `address_to` varchar(200) NOT NULL,
  `amount` decimal(40,18) NOT NULL,
  `amount_coin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `coins` (
  `coin_id` int(11) NOT NULL,
  `coin_name` varchar(50) NOT NULL,
  `coin_short` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `exchanges_accounts` (
  `account_id` int(11) NOT NULL,
  `exchange_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `api_key` varchar(100) NOT NULL,
  `api_private_key` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `exchanges_balances` (
  `account_id` int(11) NOT NULL,
  `coin_id` int(11) NOT NULL,
  `balance` decimal(40,18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `exchanges_ledgers` (
  `ledger_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `exchange_ledger_id` varchar(50) NOT NULL,
  `exchange_reference_id` varchar(50) NOT NULL,
  `unix_time` int(11) NOT NULL,
  `operation_type` int(11) NOT NULL,
  `coin_id` int(11) NOT NULL,
  `amount` decimal(40,18) NOT NULL,
  `fee` decimal(40,18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `exchanges_trades` (
  `trade_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `exchange_trade_id` varchar(50) NOT NULL,
  `exchange_order_id` varchar(50) NOT NULL,
  `base_coin_id` int(11) NOT NULL,
  `quote_coin_id` int(11) NOT NULL,
  `unix_time` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `order_type` int(11) NOT NULL,
  `price` decimal(40,18) NOT NULL,
  `cost` decimal(40,18) NOT NULL,
  `fee` decimal(40,18) NOT NULL,
  `fee_coin_id` int(11) NOT NULL,
  `volume` decimal(40,18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `wallets` (
  `wallet_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `wallet_name` varchar(50) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `wallets_addresses` (
  `address_id` int(11) NOT NULL,
  `wallet_id` int(11) NOT NULL,
  `address` varchar(100) NOT NULL,
  `comment` varchar(50) NOT NULL,
  `is_change` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `wallets_balances` (
  `wallet_id` int(11) NOT NULL,
  `coin_id` int(11) NOT NULL,
  `balance` decimal(48,18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `wallets_btc_raw_tx` (
  `raw_tx_id` int(11) NOT NULL,
  `wallet_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `wallets_eth_raw_tx` (
  `raw_tx_id` int(11) NOT NULL,
  `wallet_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `wallets_tx` (
  `tx_id` int(11) NOT NULL,
  `wallet_id` int(11) NOT NULL,
  `amount` decimal(40,18) NOT NULL,
  `fee` decimal(40,18) NOT NULL,
  `amount_coin_id` int(11) NOT NULL,
  `fee_coin_id` int(11) NOT NULL,
  `unix_time` int(11) NOT NULL,
  `operation_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


ALTER TABLE `blockchain_btc_raw_tx`
  ADD PRIMARY KEY (`raw_tx_id`);

ALTER TABLE `blockchain_btc_raw_tx_details`
  ADD PRIMARY KEY (`raw_tx_detail_id`);

ALTER TABLE `blockchain_eth_raw_tx`
  ADD PRIMARY KEY (`raw_tx_id`);

ALTER TABLE `blockchain_eth_raw_tx_details`
  ADD PRIMARY KEY (`raw_tx_detail_id`);

ALTER TABLE `exchanges_accounts`
  ADD PRIMARY KEY (`account_id`);

ALTER TABLE `exchanges_ledgers`
  ADD UNIQUE KEY `ledger_id` (`ledger_id`);

ALTER TABLE `exchanges_trades`
  ADD PRIMARY KEY (`trade_id`);

ALTER TABLE `wallets`
  ADD UNIQUE KEY `wallet_id` (`wallet_id`);

ALTER TABLE `wallets_addresses`
  ADD PRIMARY KEY (`address_id`);

ALTER TABLE `wallets_balances`
  ADD PRIMARY KEY (`wallet_id`,`coin_id`);

ALTER TABLE `wallets_tx`
  ADD PRIMARY KEY (`tx_id`);


ALTER TABLE `blockchain_btc_raw_tx`
  MODIFY `raw_tx_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `blockchain_btc_raw_tx_details`
  MODIFY `raw_tx_detail_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `blockchain_eth_raw_tx`
  MODIFY `raw_tx_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `blockchain_eth_raw_tx_details`
  MODIFY `raw_tx_detail_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `exchanges_accounts`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `exchanges_ledgers`
  MODIFY `ledger_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `exchanges_trades`
  MODIFY `trade_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `wallets`
  MODIFY `wallet_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `wallets_addresses`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `wallets_tx`
  MODIFY `tx_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;